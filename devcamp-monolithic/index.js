const express = require('express');
const app = express();
const port = 8000;
const path = require('path');

app.get("/", (request, response) => {
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(`${__dirname}/views/index.html`));
})

app.get("/about", (request, response) => {
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(`${__dirname}/views/about.html`));
})

app.get("/sitemap", (request, response) => {
  console.log(`__dirname: ${__dirname}`);
  response.sendFile(path.join(`${__dirname}/views/sitemap.html`));
})
 
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})
